#   Copyright (c) 1999-2001  Jason Gunthorpe <jgg@debian.org>
#   Copyright (c) 2005       Joey Schulze <joey@infodrom.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# GPG issues -
#  - gpgm with a status FD being fed keymaterial and other interesting
#    things does nothing.. If it could ID the keys and stuff over the
#    status-fd I could decide what to do with them. I would also like it
#    to report which key it selected for encryption (also if there
#    were multi-matches..) Being able to detect a key-revoke cert would be
#    good too.
#  - I would like to be able to fetch the comment and version fields from the
#    packets so I can tell if a signature is made by pgp2 to enable the
#    pgp2 encrypting mode.

from __future__ import print_function

import email
import email.message
import fcntl
import os
import re
import sys
import tempfile
import time

from userdir_ldap.exceptions import UDFormatError

import dbm.ndbm

# General GPG options
GPGPath = "gpg"
# "--load-extension", "rsa",
GPGBasicOptions = ["--no-options",
                   "--batch",
                   "--no-default-keyring",
                   "--secret-keyring", "/dev/null",
                   "--always-trust"]
GPGKeyRings = []
GPGSigOptions = ["--output", "-"]
GPGSearchOptions = ["--dry-run", "--with-colons", "--fingerprint",
                    "--fingerprint", "--fixed-list-mode"]
GPGEncryptOptions = ["--output", "-", "--quiet", "--always-trust",
                     "--armor", "--encrypt"]

# Replay cutoff times in seconds
CleanCutOff = 7 * 24 * 60 * 60
AgeCutOff = 4 * 24 * 60 * 60
FutureCutOff = 3 * 24 * 60 * 60


if sys.version_info[0] < 3:
    def message_as_bytes(x):
        return x.as_string()
else:
    def message_as_bytes(x):
        return x.as_bytes()


def ClearKeyrings():
   del GPGKeyRings[:]


# Set the keyrings, the input is a list of keyrings
def SetKeyrings(Rings):
   for x in Rings:
      GPGKeyRings.append("--keyring")
      GPGKeyRings.append(x)


# This opens GPG in 'write filter' mode. It takes Message and sends it
# to GPGs standard input, pipes the standard output to a temp file along
# with the status FD. The two tempfiles are passed to GPG by fd and are
# accessible from the filesystem for only a short period. Message may be
# None in which case GPGs stdin is closed directly after forking. This
# is best used for sig checking and encryption.
# The return result is a tuple (Exit,StatusFD,OutputFD), both fds are
# fully rewound and readable.
def GPGWriteFilter(Program, Options, Message):
   assert isinstance(Message, (type(None), type(b'')))
   # Make sure the tmp files we open are unreadable, there is a short race
   # between when the temp file is opened and unlinked that some one else
   # could open it or hard link it. This is not important however as no
   # Secure data is fed through the temp files.
   OldMask = os.umask(0o777)
   try:
      Output = tempfile.TemporaryFile("w+b")
      GPGText = tempfile.TemporaryFile("w+b")
      InPipe = os.pipe()
      InPipe = [InPipe[0], InPipe[1]]
   finally:
      os.umask(OldMask)

   try:
      # Fork off GPG in a horrible way, we redirect most of its FDs
      # Input comes from a pipe and its two outputs are spooled to unlinked
      # temp files (ie private)
      Child = os.fork()
      if Child == 0:  # pragma: no cover
         try:
            os.dup2(InPipe[0], 0)
            os.close(InPipe[1])
            os.dup2(Output.fileno(), 1)
            os.dup2(os.open("/dev/null", os.O_WRONLY), 2)
            os.dup2(GPGText.fileno(), 3)

            Args = [Program, "--status-fd", "3"] + GPGBasicOptions + GPGKeyRings + Options
            os.execvp(Program, Args)
         finally:
            os._exit(100)

      # Get rid of the other end of the pipe
      os.close(InPipe[0])
      InPipe[0] = -1

      # Send the message
      if Message is not None:
         os.write(InPipe[1], Message)
      os.close(InPipe[1])
      InPipe[1] = -1

      # Wait for GPG to finish
      Exit = os.waitpid(Child, 0)

      # Create the result including the new readable file descriptors
      Result = (Exit, os.fdopen(os.dup(GPGText.fileno()), "rb"),
                os.fdopen(os.dup(Output.fileno()), "rb"))
      Result[1].seek(0)
      Result[2].seek(0)

      Output.close()
      GPGText.close()
      return Result
   finally:
      if InPipe[0] != -1:
         os.close(InPipe[0])
      if InPipe[1] != -1:
         os.close(InPipe[1])
      Output.close()
      GPGText.close()


# This takes a text passage, a destination and a flag indicating the
# compatibility to use and returns an encrypted message to the recipient.
# It is best if the recipient is specified using the hex key fingerprint
# of the target, ie 0x64BE1319CCF6D393BF87FF9358A6D4EE
def GPGEncrypt(Message, To):
   class KeyringError(Exception):
      pass
   # Encrypt using the PGP5 block encoding and with the PGP5 option set.
   # This will handle either RSA or DSA/DH asymetric keys.
   searchkey = GPGKeySearch(To)
   if len(searchkey) == 0:
      raise KeyringError("No key found matching %s" % To)
   elif len(searchkey) > 1:
      raise KeyringError("Multiple keys found matching %s" % To)
   if searchkey[0][4].find("E") < 0:
      raise KeyringError("Key %s has no encryption capability - are all encryption subkeys expired or revoked?  Are there any encryption subkeys?" % To)

   Message = Message.encode('utf-8')
   try:
    Res = None
    Res = GPGWriteFilter(GPGPath, ["-r", To] + GPGEncryptOptions, Message)
    if Res[0][1] != 0:
       return None
    Text = Res[2].read().decode("utf-8")
    return Text
   finally:
    if Res is not None:
       Res[1].close()
       Res[2].close()


def gpg_check_email_sig(message, paranoid=False, lax_multipart=False):
    '''check PGP signature on an email
    message: email.message.Message, either pgp/mime or inline

    If paranoid, raise UDFormatError unless the whole message is signed

    If lax_multipart, treat multipart bodies other than multipart/signed as one
    big plain text body

    Returns a tuple consisting of a GPGCheckSig2 instance and a boolean
    indicating whether the result should be mime processed
    '''
    assert isinstance(message, email.message.Message)
    assert not (paranoid and lax_multipart)

    # See if this is a MIME encoded multipart signed message
    if message.is_multipart():
        if not message.get_content_type() == 'multipart/signed':
            if lax_multipart:
                payloads = message.get_payload()
                msg = '\n'.join(p.get_payload(decode=True) for p in payloads)
                return gpg_check_inline_sig(msg)
            raise UDFormatError("Cannot handle multipart messages not of type multipart/signed")

        if paranoid:
            if message.preamble is not None and message.preamble.strip():
                raise UDFormatError("Unsigned text in message (at start)")
            if message.epilogue is not None and message.epilogue.strip():
                raise UDFormatError("Unsigned text in message (at end)")

        payloads = message.get_payload()
        if len(payloads) != 2:
            raise UDFormatError("multipart/signed message with number of payloads != 2")

        (signed, signature) = payloads
        if signed.get_content_type() != 'text/plain' and not lax_multipart:
            raise UDFormatError("Invalid pgp/mime encoding for first part [wrong plaintext type]")
        if signature.get_content_type() != 'application/pgp-signature':
            raise UDFormatError("Invalid pgp/mime encoding for second part [wrong signature type]")

        return gpg_check_detached_sig(signature.get_payload(), message_as_bytes(signed)), True

    payload = message.get_payload(decode=True)
    if paranoid:
        if b'-----BEGIN PGP SIGNED MESSAGE-----' not in payload:
            raise UDFormatError("No PGP signature")

        # make sure the message includes nothing but signed text and signature
        body = []
        state = 1
        for line in payload.split(b'\n'):
            body.append(line)
            if not line.strip():
                continue
            # before the signed message
            if state == 1:
                if line != b'-----BEGIN PGP SIGNED MESSAGE-----':
                    raise UDFormatError("Unsigned text in message (at start)")
                state = 2
                continue

            # in the signed message
            if state == 2:
                if line == b'-----BEGIN PGP SIGNATURE-----':
                    state = 3
                continue

            # in the signature
            if state == 3:
                if line == b'-----END PGP SIGNATURE-----':
                    state = 4
                continue

            # epilogue
            if state == 4:
                raise UDFormatError("Unsigned text in message (at end)")

    return gpg_check_inline_sig(payload), False


def gpg_check_inline_sig(msg):
    return GPGCheckSig2(msg)


def gpg_check_detached_sig(signature, signed):
    sigfile = tempfile.NamedTemporaryFile(mode='w', delete=False)
    try:
        sigfile.write(signature)
        sigfile.close()
        return GPGCheckSig2(signed, sigfile.name)
    finally:
        os.unlink(sigfile.name)


# Checks the signature of a standard PGP message, either clearsigned or with a
# detached signature. It returns a large tuple of the form:
#   (Why,(SigId,Date,KeyFinger),(KeyID,KeyFinger,Owner),Text);
# Where,
#  Why = None if checking was OK otherwise an error string.
#  SigID+Date represent something suitable for use in a replay cache. The
#             date is returned as the number of seconds since the UTC epoch.
#             The keyID is also in this tuple for easy use of the replay
#             cache
#  KeyID, KeyFinger and Owner represent the Key used to sign this message
#  Text is the full byte-for-byte signed text in a string
def GPGCheckSig(Message, sigfile=None):
   if sigfile is None:
       Res = GPGWriteFilter(GPGPath, GPGSigOptions, Message)
   else:
       Text = Message.replace(b'\r\n', b'\n').replace(b'\n', b'\r\n')
       Res = GPGWriteFilter(GPGPath, ['--verify', sigfile, '-'], Text)
   try:
      Exit = Res[0]

      # Parse the GPG answer
      Strm = Res[1]
      GoodSig = 0
      SigId = None
      KeyFinger = None
      KeyID = None
      Owner = None
      Date = None
      Why = None
      TagMap = {}
      while True:
         # Grab and split up line
         Line = Strm.readline()
         if Line == b"":
            break
         try:
             Line = Line.decode("utf-8")
         except UnicodeDecodeError:
             # some binary thing, possibly salt@notations.sequoia-pgp.org
             # https://sequoia-pgp.org/blog/2021/01/26/202101-sq-release/
             # https://gitlab.com/decathorpe/sequoia-chameleon-gnupg#collision-protection
             continue
         Split = re.split("[ \n]", Line)
         if Split[0] != "[GNUPG:]":
            continue

         # We only process the first occurance of any tag.
         if Split[1] in TagMap:
            continue
         TagMap[Split[1]] = None

         # Good signature response
         if Split[1] == "GOODSIG":
            # Just in case GPG returned a bad signal before this (bug?)
            if Why is None:
               GoodSig = 1
            KeyID = Split[2]
            Owner = ' '.join(Split[3:])
            # If this message is signed with a subkey which has not yet
            # expired, GnuPG will say GOODSIG here, even if the primary
            # key already has expired.  This came up in discussion of
            # bug #489225.  GPGKeySearch only returns non-expired keys.
            Verify = GPGKeySearch(KeyID)
            if len(Verify) == 0:
               GoodSig = 0
               Why = "Key has expired (no unexpired key found in keyring matching %s)" % KeyID

         # Bad signature response
         if Split[1] == "BADSIG":
            GoodSig = 0
            KeyID = Split[2]
            Why = "Verification of signature failed"

         # Bad signature response
         if Split[1] == "ERRSIG":
            GoodSig = 0
            KeyID = Split[2]
            if len(Split) <= 7:
               Why = "GPG error, ERRSIG status tag is invalid"
            elif Split[7] == '9':
               Why = "Unable to verify signature, signing key missing."
            elif Split[7] == '4':
               Why = "Unable to verify signature, unknown packet format/key type"
            else:
               Why = "Unable to verify signature, unknown reason"

         if Split[1] == "NO_PUBKEY":
            GoodSig = 0
            Why = "Unable to verify signature, signing key missing."

         # Expired signature
         if Split[1] == "EXPSIG":
            GoodSig = 0
            Why = "Signature has expired"

         # Expired signature
         if Split[1] == "EXPKEYSIG":
            GoodSig = 0
            Why = "Signing key (%s, %s) has expired" % (Split[2], Split[3])

         # Revoked key
         if Split[1] == "KEYREVOKED" or Split[1] == "REVKEYSIG":
            GoodSig = 0
            Why = "Signing key has been revoked"

         # Corrupted packet
         if Split[1] == "NODATA" or Split[1] == "BADARMOR":
            GoodSig = 0
            Why = "The packet was corrupted or contained no data"

         # Signature ID
         if Split[1] == "SIG_ID":
            SigId = Split[2]
            Date = int(Split[4])

         # ValidSig has the key finger print
         if Split[1] == "VALIDSIG":
            # Use the fingerprint of the primary key when available
            if len(Split) >= 12:
               KeyFinger = Split[11]
            else:
               KeyFinger = Split[2]

      # Reopen the stream as a readable stream
      if sigfile is None:
          Text = Res[2].read().decode("utf-8")

      # A gpg failure is an automatic bad signature
      if Exit[1] != 0 and Why is None:
         GoodSig = 0
         Why = "GPG execution returned non-zero exit status: " + str(Exit[1])

      if GoodSig == 0 and (Why is None or len(Why) == 0):
         Why = "Checking Failed"

      return (Why, (SigId, Date, KeyFinger), (KeyID, KeyFinger, Owner), Text)
   finally:
      Res[1].close()
      Res[2].close()


class GPGCheckSig2:
    def __init__(self, msg, sig=None):
        res = GPGCheckSig(msg, sig)
        self.why = res[0]
        self.sig_info = res[1]
        self.key_info = res[2]
        self.text = res[3]

        self.ok = self.why is None

        self.sig_id = self.sig_info[0]
        self.sig_date = self.sig_info[1]
        self.sig_fpr = self.sig_info[2]

        self.key_id = self.key_info[0]
        self.key_fpr = self.key_info[1]
        self.key_owner = self.key_info[2]

    def __eq__(self, other):
        return self.why == other.why and self.sig_info == other.sig_info and self.key_info == other.key_info and self.text == other.text

    def __str__(self):
        return '%s(%s, %s, %s, %s)' % (self.__class__.__name__, self.why, self.sig_info, self.key_info, repr(self.text))

    def __repr__(self):
        return str(self)


# Search for keys given a search pattern. The pattern is passed directly
# to GPG for processing. The result is a list of tuples of the form:
#   (KeyID,KeyFinger,Owner,Length)
# Which is similar to the key identification tuple output by GPGChecksig
#
# Do not return keys where the primary key has expired
def GPGKeySearch(SearchCriteria):
   Args = [GPGPath] + GPGBasicOptions + GPGKeyRings + GPGSearchOptions + \
          [SearchCriteria, " 2> /dev/null"]
   Strm = None
   Result = []
   Validity = None
   Length = 0
   KeyID = ""
   Capabilities = ""
   Fingerprint = ""
   Owner = ""
   Hits = set()

   dir = os.path.expanduser("~/.gnupg")
   if not os.path.isdir(dir):
      os.mkdir(dir, 0o700)

   try:
      # The GPG output will contain zero or more stanza, one stanza per match found.
      # Each stanza consists of the following records, in order:
      #   tru : trust database information
      #   pub : primary key from which we extract
      #         field  1 - Validity
      #         field  2 - Length
      #         field  4 - KeyID
      #         field 11 - Capabilities
      #   fpr : fingerprint of primary key from which we extract
      #         field  9 - Fingerprint
      #   uid : first User ID attached to primary key from which we extract
      #         Field  9 - Owner
      #   uid : (optional) additional multiple User IDs attached to primary key
      #   sub : (optional) secondary key
      #   fpr : (opitonal) fingerprint of secondary key if sub is present
      Strm = os.popen(" ".join(Args), "r")
      Want = "pub"
      while True:
         Line = Strm.readline()
         if Line == "":
            break
         Split = Line.split(":")

         if Split[0] != Want:
            continue

         if Want == 'pub':
            Validity = Split[1]
            Length = int(Split[2])
            KeyID = Split[4]
            Capabilities = Split[11]
            Want = 'fpr'
            continue

         if Want == 'fpr':
            Fingerprint = Split[9]
            if Fingerprint in Hits:
               Want = 'pub'  # already seen, skip to next stanza
            else:
               Hits.add(Fingerprint)
               Want = 'uid'
            continue

         if Want == 'uid':
            Owner = Split[9]
            if Validity != 'e':  # if not expired
               Result.append((KeyID, Fingerprint, Owner, Length, Capabilities))
            Want = 'pub'  # finished, skip to next stanza
            continue

   finally:
      if Strm is not None:
         Strm.close()
   return Result


# Print the available key information in a format similar to GPG's output
# We do not know the values of all the feilds so they are just replaced
# with ?'s
def GPGPrintKeyInfo(Ident):
   print("pub  %u?/%s ??-??-?? %s" % (Ident[3], Ident[0][-8:], Ident[2]))
   print("     key fingerprint = 0x%s" % Ident[1])


# Perform a substition of template
def TemplateSubst(Map, Template):
   for x in Map.keys():
      Template = Template.replace(x, Map[x])
   return Template


# The replay class uses a python DB (BSD db if avail) to implement
# protection against replay. Replay is an attacker capturing the
# plain text signed message and sending it back to the victim at some
# later date. Each signature has a unique signature ID (and signing
# Key Fingerprint) as well as a timestamp. The first stage of replay
# protection is to ensure that the timestamp is reasonable, in particular
# not to far ahead or too far behind the current system time. The next
# step is to look up the signature + key fingerprint in the replay database
# and determine if it has been recived. The database is cleaned out
# periodically and old signatures are discarded. By using a timestamp the
# database size is bounded to being within the range of the allowed times
# plus a little fuzz. The cache is serialized with a flocked lock file
class ReplayCache:
   def __init__(self, Database):
      self.Lock = open(Database + ".lock", "w")
      fcntl.flock(self.Lock.fileno(), fcntl.LOCK_EX)
      self.DB = dbm.ndbm.open(Database, "c", 0o600)
      self.CleanCutOff = CleanCutOff
      self.AgeCutOff = AgeCutOff
      self.FutureCutOff = FutureCutOff
      self.Clean()

   # Close the cache and lock
   def ___del__(self):
      self.close()

   def close(self):
      self.DB.close()
      self.Lock.close()

   # Clean out any old signatures
   def Clean(self):
      CutOff = time.time() - self.CleanCutOff
      for x in self.DB.keys():
         if int(self.DB[x]) <= CutOff:
            del self.DB[x]

   # Check a signature. 'sig' is a 3 tuple that has the sigId, date and
   # key ID
   def Check(self, Sig):
      if Sig[0] is None or Sig[1] is None or Sig[2] is None:
         return "Invalid signature"
      if int(Sig[1]) > time.time() + self.FutureCutOff:
         return "Signature has a time too far in the future"
      if (Sig[0] + '-' + Sig[2]) in self.DB:
         return "Signature has already been received"
      if int(Sig[1]) < time.time() - self.AgeCutOff:
         return "Signature has passed the age cut off "
      # + str(int(Sig[1])) + ',' + str(time.time()) + "," + str(Sig)
      return None

   # Add a signature, the sig is the same as is given to Check
   def Add(self, Sig):
      if Sig[0] is None or Sig[1] is None:
         raise RuntimeError("Invalid signature")
      if Sig[1] < time.time() - self.CleanCutOff:
         return
      Key = Sig[0] + '-' + Sig[2]
      if Key in self.DB:
         if int(self.DB[Key]) < Sig[1]:
            self.DB[Key] = str(int(Sig[1]))
      else:
         self.DB[Key] = str(int(Sig[1]))

   def process(self, sig_info):
      r = self.Check(sig_info)
      if r is not None:
         raise RuntimeError("The replay cache rejected your message: %s." % r)
      self.Add(sig_info)
      self.close()

# vim:set et:
# vim:set ts=3:
# vim:set shiftwidth=3:
