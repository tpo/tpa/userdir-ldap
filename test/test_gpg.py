#!/usr/bin/python

from __future__ import print_function

import email.parser
import email.policy
import os
import shutil
import subprocess
import sys
import tempfile
import unittest

from userdir_ldap.exceptions import UDFormatError
import userdir_ldap.gpg as userdir_gpg


data_dir = os.path.join(os.path.dirname(__file__), 'data')


class GPGTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.gnupghome = tempfile.mkdtemp()
        os.environ['GNUPGHOME'] = cls.gnupghome
        # XXX this should probably not be hardcoded as it has an expiration date
        # XXX also we should have tests for the "signed with expired key" case anyway
        with open(os.path.join(data_dir, 'private_key.asc'), 'rb') as pkey:
            cmd = subprocess.Popen(['gpg', '--import'],
                                   stdin=pkey,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = cmd.communicate()
        assert cmd.wait() == 0, stderr

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.gnupghome)

    def test_checksig_inline(self):
        payload = '''\
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

this is a test message
-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEEmUT71Nbnvig80uYjNpcvOIy+7xcFAl+YKdgACgkQNpcvOIy+
7xd+mwwAwJ4oXXEdtoHhSxqGGE97DTA/vZn32XzLKxfWBZRkohRbWTyyfTRwGbH3
ER/X5z726kZzFss98Ei5qu0Nn6ZA99349gFcPLiofM9nQhJJ3AE7k1dX2gJynGa9
MBUixVZJE5mTUtw9tRKng2+4eV7aMP61iIUa7A+F8vQTWIkJNcJyTPV4ZoSfLKCS
eML8jBjKR7LNiT/mUx84uhjRZENjuEDw1/pPvowBaz6XqAeo3L5lHpdRgMw9tQtv
l0VHbc2WYTptSnSSmnFC0hFNDFvE/jcQR7jXwBBQEGnqAEc/7iN46/NPTwz7FruC
scS8RxKRsX8y1YoVXsA4QQ2cYt+/11qlN1DB+afvrYP9cB0PU/68QmqFvRBj/yw+
ki/1H0WecXpMa0sRirWZ+VkC825FtGa4lhBwlziWmGTsCnen910zaqKFasPY6QFZ
L7agRITRLffpMhDaiFUs7SdP11heiRDFjd+s+4dSO+TPlysUM7d54r7UamPuFqNy
Ux3bY8DP
=Hyyy
-----END PGP SIGNATURE-----
'''
        userdir_gpg.ClearKeyrings()
        mail = email.parser.Parser().parsestr(payload)
        res, mime = userdir_gpg.gpg_check_email_sig(mail, paranoid=True)
        self.assertTrue(res.ok)
        self.assertEqual('this is a test message\n', res.text)
        self.assertFalse(mime)

        mail = email.parser.Parser().parsestr('some extra text\n' + payload)
        res, mime = userdir_gpg.gpg_check_email_sig(mail, paranoid=False)
        self.assertTrue(res.ok)
        self.assertEqual('this is a test message\n', res.text)
        self.assertFalse(mime)
        with self.assertRaises(UDFormatError):
            userdir_gpg.gpg_check_email_sig(mail, paranoid=True)

        mail = email.parser.Parser().parsestr(payload + 'some extra text\n')
        res, mime = userdir_gpg.gpg_check_email_sig(mail, paranoid=False)
        self.assertTrue(res.ok)
        self.assertEqual('this is a test message\n', res.text)
        self.assertFalse(mime)
        with self.assertRaises(UDFormatError):
            userdir_gpg.gpg_check_email_sig(mail, paranoid=True)

    def test_checksig_mime(self):
        with open(os.path.join(data_dir, 'good_mime'), 'r') as f:
            payload = f.read()
        signed_text = b'''\
Content-Type: text/plain; charset=us-ascii\r
Content-Disposition: inline\r
\r
This is a test message.\r
'''
        userdir_gpg.ClearKeyrings()
        mail = email.parser.Parser().parsestr(payload)
        res, mime = userdir_gpg.gpg_check_email_sig(mail, paranoid=True)
        self.assertTrue(res.ok)
        self.assertEqual(signed_text, res.text)
        self.assertTrue(mime)

    def test_unsigned(self):
        payload = '''\
This is not a signed message
'''
        mail = email.parser.Parser().parsestr(payload)
        with self.assertRaises(UDFormatError):
            userdir_gpg.gpg_check_email_sig(mail, paranoid=True)

    def test_badsig(self):
        payload = '''\
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

this is a test
message
-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEEmUT71Nbnvig80uYjNpcvOIy+7xcFAl+YKdgACgkQNpcvOIy+
7xd+mwwAwJ4oXXEdtoHhSxqGGE97DTA/vZn32XzLKxfWBZRkohRbWTyyfTRwGbH3
ER/X5z726kZzFss98Ei5qu0Nn6ZA99349gFcPLiofM9nQhJJ3AE7k1dX2gJynGa9
MBUixVZJE5mTUtw9tRKng2+4eV7aMP61iIUa7A+F8vQTWIkJNcJyTPV4ZoSfLKCS
eML8jBjKR7LNiT/mUx84uhjRZENjuEDw1/pPvowBaz6XqAeo3L5lHpdRgMw9tQtv
l0VHbc2WYTptSnSSmnFC0hFNDFvE/jcQR7jXwBBQEGnqAEc/7iN46/NPTwz7FruC
scS8RxKRsX8y1YoVXsA4QQ2cYt+/11qlN1DB+afvrYP9cB0PU/68QmqFvRBj/yw+
ki/1H0WecXpMa0sRirWZ+VkC825FtGa4lhBwlziWmGTsCnen910zaqKFasPY6QFZ
L7agRITRLffpMhDaiFUs7SdP11heiRDFjd+s+4dSO+TPlysUM7d54r7UamPuFqNy
Ux3bY8DP
=Hyyy
-----END PGP SIGNATURE-----
'''
        userdir_gpg.ClearKeyrings()
        mail = email.parser.Parser().parsestr(payload)
        res, mime = userdir_gpg.gpg_check_email_sig(mail, paranoid=True)
        self.assertFalse(res.ok)
        self.assertEqual(res.why, "Verification of signature failed")
        self.assertFalse(mime)

    def test_encrypt(self):
        encrypted = userdir_gpg.GPGEncrypt(
            u"This is an encrypted message.\n",
            "0x9944FBD4D6E7BE283CD2E62336972F388CBEEF17")
        self.assertTrue(encrypted.startswith('-----BEGIN PGP MESSAGE-----\n'), encrypted)
        with self.assertRaises(Exception) as cm:
            userdir_gpg.GPGEncrypt(u"This is an encrypted message.\n", "0xDEADBEEF")
        self.assertEqual(cm.exception.args, ("No key found matching 0xDEADBEEF", ))

    def test_sigcheck(self):
        with open(os.path.join(data_dir, 'good_mime'), 'rb') as f:
            cmd = subprocess.Popen(
                [sys.executable, os.path.join(os.path.dirname(__file__), '..', 'sigcheck')],
                stdin=f, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = cmd.communicate()
        self.assertEqual(cmd.wait(), 0)
        self.assertEqual(stdout, b'Inspecting message <something.unique@example.com>\nMessage <something.unique@example.com> passed\n')
        self.assertEqual(stderr, b'')

    def test_checksig_mime_tb(self):
        with open(os.path.join(data_dir, 'good_mime_tb'), 'r') as f:
            payload = f.read()
        userdir_gpg.ClearKeyrings()
        mail = email.parser.Parser(policy=email.policy.HTTP).parsestr(payload)
        res, mime = userdir_gpg.gpg_check_email_sig(mail, lax_multipart=True)
        self.assertEqual(
            res.text,
            b'Content-Type: multipart/mixed; boundary="------------ef2ZumxyUd90vUw1hnJ2FpXB";\r\n protected-headers="v1"\r\nFrom: foo@example.com\r\nTo: bar@example.com\r\nMessage-ID: <f20d26b9-a682-415c-9eb8-5178a8c92900@riseup.net>\r\nSubject: Test message\r\n\r\n--------------ef2ZumxyUd90vUw1hnJ2FpXB\r\nContent-Type: text/plain; charset=UTF-8; format=flowed\r\nContent-Transfer-Encoding: base64\r\n\r\nVGhpcyBpcyBhIHRlc3QgbWVzc2FnZS4NCg0K\r\n\r\n--------------ef2ZumxyUd90vUw1hnJ2FpXB--\r\n',  # noqa: E501
        )
        self.assertTrue(res.ok)
        self.assertTrue(mime)


if __name__ == '__main__':
    unittest.main()

# vim: set et sw=4 sts=4:
